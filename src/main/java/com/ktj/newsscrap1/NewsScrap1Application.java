package com.ktj.newsscrap1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsScrap1Application {

    public static void main(String[] args) {
        SpringApplication.run(NewsScrap1Application.class, args);
    }

}
